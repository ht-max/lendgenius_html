;
(function ($) {


    $(document).ready(function () {

        $('.subpage_menu .mobile_menu_icon').click(function () {
            $('.subpage_menu .menu').toggle("slow");
        });

        $('.content-blog-items').masonry({
            itemSelector: '.blog-item',
            columnWidth: 20,
            layoutInstant: false
        });


        $('a[href*=#]').bind("click", function (e) {
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top
            }, 1000);
            e.preventDefault();
        });
        return false;
    });


    // $('.partner1 select').styler();

}(jQuery));
