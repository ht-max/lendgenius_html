;
//=require _jquery-1.11.1.min.js
;
//=require _bootstrap.js
;
//=require _jquery.formstyler.js
;
//=require _main.js
;
$(document).ready(function () {
   $('[type="reset"]').click( function (e) {
      setTimeout(function() {
         $('select.field-styler-option').trigger('refresh');
      }, 1)
   });
   $('select.field-styler-option').styler().trigger('refresh');
})
;