const gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    pug = require('gulp-pug'),
    autoprefixer = require('gulp-autoprefixer'),
    // uncss = require('gulp-uncss-task'),
    uglify = require('gulp-uglify'),
    include       = require("gulp-include"),
    // del = require("del"),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    zip = require('gulp-zip'),
    iconfont = require('gulp-iconfont'),
    iconfontCss = require('gulp-iconfont-css'),
    iconfontHtml = require('gulp-iconfont-template'),
    spritesmith = require('gulp.spritesmith');


;


var root = "gitlab/lpmarkup/lpmarkup/";
var conf = {
    src: "./src",
    build: {
        output: "output/",
        fonts: "output/assets/fonts/",
        images: "output/assets/images/",
        scripts: "output/assets/js/",
        styles: "output/assets/styles/"
    },
    wp: {
        root: root,
        output: root,
        fonts: root + 'assets/fonts/',
        images: root + 'assets/images/',
        scripts: root + 'assets/js/',
        styles: root + 'assets/styles/'
    }
};

gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: conf.wp.root
        },
    });
});

gulp.task('pug', function () {
    gulp.src([conf.src + '/**/*.pug', '!' + conf.src + '/**/_*.pug'])
        .pipe(pug({
            pretty: true
        }).on('error', function (err) {
            console.log(err);
        }))
        .pipe(gulp.dest(conf.wp.output)).on('end', browserSync.reload);
});

gulp.task('scripts', function () {
    gulp.src([conf.src + '/**/*.js', '!' + conf.src + '/**/_*.js'])
        .pipe(include())
        // .pipe(uglify())
        .pipe(gulp.dest(conf.wp.output)).on('end', browserSync.reload)

});

gulp.task('images', function () {
    gulp.src([
        conf.src + '/assets/images/**/*.*',
        '!' + conf.src + '/assets/images/sprite/*'
    ])
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(conf.wp.images));

}).on('end', browserSync.reload);


gulp.task('images_sprite', function () {
    gulp.src(conf.src + '/assets/images/sprite/**/*.png')
        .pipe(spritesmith({
            imgName: '../images/sprite.png',
            cssName: '_sprite.sass',
            cssFormat: 'sass',
            algorithm: 'binary-tree',
        }))
        .pipe(gulp.dest(conf.src + '/assets/styles/'))
        .on('end', function () {
            gulp.run('images');
        })

}).on('end', browserSync.reload);



gulp.task('styles', function () {
    return gulp.src([conf.src + '/**/*.sass', conf.src + '/**/*.scss' ])
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: false

        }).on('error', sass.logError))

        .pipe(autoprefixer())
        .pipe(sourcemaps.write('./', {addComment: false, sourceRoot: './'}))


        .pipe(gulp.dest(conf.wp.output)).on('end', browserSync.reload);
});
gulp.task('fonts', function () {
    gulp.src(conf.src + '/assets/fonts/**/*.*')
        .pipe(gulp.dest(conf.wp.fonts));
});

gulp.task('iconfont', function(){
    gulp.src('src/assets/icon-font/*.svg')
        // .pipe(iconfontHtml({
        //     fontName: 'SvgIcons',
        //     // path: conf.src + '/svg-icon.pug',
        //     targetPath: '../../../output/svg-icons.html',
        // }))
        .pipe(iconfontCss({
            fontName: 'SvgIcons',
            // path: './src/assets/styles/_icon-font.css',
            targetPath: '../styles/_svg-fonts.scss',
            fontPath: '../fonts/',
            cssClass: 'font-icon'
        }))
        .pipe(iconfont({
            fontName: 'SvgIcons',
            formats: ['eot', 'svg', 'ttf', 'woff', "woff2"],
            prependUnicode: true,
            normalize: true,
            // fixedWidth: false,
            // round: 0,
            // centerhorizontally: true,
            timestamp: Math.round(Date.now()/1000)
        }))
        .pipe(gulp.dest('src/assets/fonts/'))
        .on('end', function () {
            gulp.run('fonts');
            gulp.run('styles');
        });
});


gulp.task('zip', function () {
   gulp.src([ conf.wp.output+'/**/*', '!'+ conf.wp.output + '/*.zip'])
       .pipe(zip('output.zip'))
       .pipe(gulp.dest(conf.wp.output))
       .on('end', function () {
           console.log("ZIP COMPLETED")
       })
});




gulp.task('watch', function () {
    gulp.watch([conf.src + '/**/*.sass'], ['styles']);
    gulp.watch([conf.src + '/**/*.pug'], ['pug']);
    gulp.watch([conf.src + '/**/*.js'], ['scripts']);
    gulp.watch([conf.src + '/assets/fonts/*'], ['scripts']);
    gulp.watch(['src/assets/images/**/*.*', '!src/assets/images/sprite/*'], ['images']);
    gulp.watch(['src/assets/images/sprite/*.*'], ['images_sprite']);
    gulp.watch(['src/assets/icon-font/**/*.svg'], ['iconfont']);

});

gulp.task('build', ['pug', 'scripts', 'styles', 'images', 'images_sprite', 'iconfont', 'fonts']);

gulp.task('default', ['build', 'watch', 'serve']);
